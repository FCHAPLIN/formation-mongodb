const mongoose = require("mongoose")

const checkSchema = new mongoose.Schema({
    name: String,
    names: String
});

const Check = mongoose.model('check', checkSchema);

module.exports = Check;
