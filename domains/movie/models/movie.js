const mongoose = require("mongoose")

const movieSchema = new mongoose.Schema({
    title: String,
    year: Number,
    num_mflix_comments: Number
});

const Movie = mongoose.model('movie', movieSchema);

module.exports = Movie;