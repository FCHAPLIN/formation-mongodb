var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const Check = require('./domains/check/models/check.js')
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
mongoose
    .connect('mongodb+srv://jonsnow:got34@atlascluster.sgfrlhp.mongodb.net/sample_mflix?retryWrites=true&w=majority')
    .then(() => {
      console.log(process.env.BBD_CONNEXION_STRING)
      console.log('MongoDB is connected')
        Check.watch().on('change', (event) => { console.log(event)} )
    })
    .catch((err) => {
      console.log('MongoDB connection unsuccessful, retry after 5 seconds.')
      console.log(err)
      setTimeout(connectDatabase, 5000)
    })


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
